package com.example;

public class Fibonacci {

    public static long compute(int n) {
        long previous = 0;
        long result = 1;
        for (int i = 0; i < n; i++) {
            final long temp = result;
            result = result + previous;
            previous = temp;
        }
        return result;
    }
}
