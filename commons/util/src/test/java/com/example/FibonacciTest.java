package com.example;

import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

public class FibonacciTest {

    @Test
    public void testFibonacci() {
        // could also use parametrized test...
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(Fibonacci.compute(0)).isEqualTo(1);
        softly.assertThat(Fibonacci.compute(1)).isEqualTo(1);
        softly.assertThat(Fibonacci.compute(2)).isEqualTo(2);
        softly.assertThat(Fibonacci.compute(3)).isEqualTo(3);
        softly.assertThat(Fibonacci.compute(4)).isEqualTo(5);
        softly.assertThat(Fibonacci.compute(5)).isEqualTo(8);
        softly.assertAll();
    }

}