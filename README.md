# Java Maven template

Project template for a multi-module mavenproject with a lot of java code quality checks enabled.

This is a collection of personal preferences of how to use Maven.


## Maven tasks

```
   # List all Maven plugin goals in this project
   n/a   # because maven sucks

   # runs over all subprojects and displays dependencies
   mvn compile dependency:tree    # needs 'compile' because maven sucks

   # standard task, with this project runs tests, error-prone, checkstyle, pmd, spotBugs, jacoco
   mvn verify

   # checks online for updated versions to dependencies
   mvn versions:display-dependency-updates
   mvn versions:display-plugin-updates

   # creates mutation coverage report (as html, must be interpreted by humans)
   TODO
```

## Maven style

* Use BOM


## Java code style

* Break the build
  * On java compile warnings
  * On Code linters warning
 * On insufficient code coverage from unit tests
* Reuse apache commons and guava
* Use code generators for bean operations
* Use spotbugs to detect NPE potential during compile time
* Limit the scope for allowed imports

## TODO

* error prone
* spotbugs switch
* pitest
* integration test switch
* shade plugin
* lombok?
* mapstruct
* AutoValues/Immutables/Freebuilder
* Spring dependency management
* Java 10 with modules
