package com.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.Charset;
import java.util.Scanner;

public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    /**
     * Runs Fibonacci CLI
     */
    public static void main(String[] args) {
        logger.info("Enter index of Fibonacci number to compute");
        try (Scanner in = new Scanner(System.in, Charset.defaultCharset().name())) {
            final int num = in.nextInt();
            if (logger.isInfoEnabled()) {
                logger.info("Result: " + Fibonacci.compute(num));
            }
        }
    }
}
